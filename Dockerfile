FROM node:12-alpine

WORKDIR /usr/src/nodeapp

COPY ./ ./

EXPOSE 3000

CMD [ "npm" , "start" ]
